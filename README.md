# Scandiweb Junior Test v2.2

## Setup

1) Import database schema from 'db.sql'.

2) Adjust 'config.php' if needed (left it out from '.gitignore' on purpose).

3) Run 'composer dump-autoload'.

