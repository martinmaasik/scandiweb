<?php
use routing\Router;

define('ROOT', __DIR__);

require ROOT."/vendor/autoload.php";
require ROOT."/application/routing/routes.php";

Router::dispatch();
