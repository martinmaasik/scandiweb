<?php namespace database;

use PDO;

class Connection
{
    private static $connection;

    public static function getInstance() {
        if (!self::$connection) {
            include 'config.php';
            $host = $config['DB_HOST'];
            $dbname = $config['DB_DATABASE'];
            try {
                self::$connection =  new PDO("mysql:host=$host;dbname=$dbname",$config['DB_USERNAME'],$config['DB_PASSWORD']);
                self::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                self::$connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            } catch(\PDOException $e) {
                echo "Error:".$e->getMessage();
            }
        }
        return self::$connection;
    }
}
