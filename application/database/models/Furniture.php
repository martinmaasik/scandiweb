<?php namespace models;

class Furniture extends Product
{
    protected $table;
    protected $attributes = ['SKU' => '',
                            'name' => '',
                            'price' => '',
                            'height' => '',
                            'width' => '',
                            'length' => ''];

    public function __construct($request)
    {
        $this->table = 'furniture';
        parent::__construct($request);
    }
}
