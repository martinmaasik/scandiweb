<?php namespace models;

use PDO;
use database\Connection;

class Model
{
    protected $db; // database connection (singleton)
    protected $table;
    protected $attributes;
    protected $product_tables;

    public function __construct() {
        $this->db = Connection::getInstance();
        $this->product_tables = ['DVDs', 'books', 'furniture'];
    }

    public function insert() {
        $this->attributes["SKU"] = $this->uniqueSKU($this->attributes["SKU"]); // appends index to SKU to make it unique
        $prep = array();
        foreach($this->attributes as $key => $value ) {
            $prep[':'.$key] = $value;
        }
        try {
            $statement = $this->db->prepare("INSERT INTO $this->table (" . implode(', ',array_keys($this->attributes)) . ") VALUES (" . implode(', ',array_keys($prep)) . ")");
            $statement->execute($prep);
        } catch (\PDOException $e) {
            return array("error" => $e->getMessage());
        }
        return array("success" => true);
    }

    public function getAllRows() {
        $statement = $this->db->prepare('SELECT * FROM '.$this->table);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function getAllProductRows() {
        $result = [];
        foreach($this->product_tables as $table) {
            $this->table = $table;
            $result[$table] = $this->getAllRows();
        }
        return $result;
    }

    public function getAllProductSKUs() {
        $result = [];
        foreach($this->product_tables as $table) {
            $statement = ($this->db)->prepare('SELECT SKU FROM '.$table);
            $statement->execute();
            $result[] = $statement->fetchAll(PDO::FETCH_COLUMN);
        }
        $result = array_merge(...$result);
        return $result;
    }

    public function uniqueSKU($input_SKU) {
        $index = 0;                                       // if there was no SKU previously with the entered letter combination, we will append 0;
        $length = strlen($input_SKU);
        foreach ($this->getAllProductSKUs() as $SKU) {
            if (substr($SKU, 0, $length) === $input_SKU) { // check if the SKU in database starts with the same letter combination
                $suffix = substr($SKU, $length);           // get the remaining substring (suffix) of the database entry that matches our combination
                if (ctype_digit($suffix)) {                // if the suffix contains only numbers then we have a "real match"
                    if ($suffix >= $index) {               // new index has to be larger than the suffix of any of the previous db entries
                        $index = $suffix+1;
                    }
                }
            }
        }
        $uniqueSKU = $input_SKU.$index;
        return $uniqueSKU;
    }

    public function delete($request) {
        foreach($request as $table => $deletable) {
            $query = "DELETE FROM $table WHERE SKU in (".str_repeat("?,", count($deletable) - 1)."?)";
            $statement = $this->db->prepare($query);
            $statement->execute($deletable);
        }
    }
}
