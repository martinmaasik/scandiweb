<?php namespace models;

class Book extends Product
{
    protected $table;
    protected $attributes = ['SKU' => '',
                            'name' => '',
                            'price' => '',
                            'weight' => ''];

    public function __construct($request)
    {
        $this->table = 'books';
        parent::__construct($request);
    }
}
