<?php namespace models;

abstract class Product extends Model
{
    protected $attributes = ['SKU' => '',
                            'name' => '',
                            'price' => ''];

    public function __construct($request) {
        parent::__construct();
        foreach($this->attributes as $key => $value ) {
            $this->attributes[$key] = $request[$key];
        }
    }
}
