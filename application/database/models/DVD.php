<?php namespace models;

class DVD extends Product
{
    protected $table;
    protected $attributes = ['SKU' => '',
                            'name' => '',
                            'price' => '',
                            'size' => ''];

    public function __construct($request)
    {
        $this->table = 'DVDs';
        parent::__construct($request);
    }
}
