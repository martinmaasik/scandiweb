$(document).ready(function(){
    $("input[name='productType']").click(function(){
        var radioValue = $("input[name='productType']:checked").val();
        if(radioValue){
          $('#attribute >*').hide();
          $('#' +radioValue).show();
          $('#attribute').find('input').attr('disabled', true);
          $('#' +radioValue).find('input').attr('disabled', false);
        }
    });
});
