<?php
namespace frontend;

class View
{
    private $directory;
    private $data = array();

    public function __construct($template, array $data) {
        $this->directory = ROOT.'/application/frontend/views/'.$template.'.html';
        $this->data = $data;
    }

    public function render() {
        ob_start();
        extract($this->data);
        require($this->directory);
        echo ob_get_clean();
    }
}
