<?php
namespace routing;
use controllers;

class Router
{
    // setting up all the routes from routes.php file during bootstrap
    static private $routes = array();

    public static function get($route, $controller) {
        self::addRoute('get', $route, $controller);
    }

    public static function post($route, $controller) {
        self::addRoute('post', $route, $controller);
    }

    private static function addRoute($method, $path, $controller) {
        $route = [];
        if (strpos($path, '/:') !== false) {
            $parse = explode(':', $path);
            $route['path'] = substr($parse[0], 0, -1);
            $route['param'] = true;
        } else {
            $route['path'] = $path;
        }
        $route['method'] = strtoupper($method);

        if (strpos($controller, '@') !== false) {
            $parse = explode('@', $controller);
            $route['controller'] = $parse[0];
            $route['action'] = $parse[1];
        } else {
            $route['controller'] = $controller;
            $route['action'] = 'index';
        }
        self::$routes[] = $route;
    }

    public static function dispatch() {
        $uri = $_SERVER['REQUEST_URI'];
        $path = substr($uri, strpos($uri, '/', 1));
        $method = $_SERVER["REQUEST_METHOD"];
        foreach (self::$routes as $route) {
            $action = $route['action'];
            if ($route['path'] == $path && $route['method'] == $method) {
                $name = 'controllers\\'.$route['controller'];
                $controller = new $name();
                if ($method == 'POST') {
                    return $controller->$action($_POST);
                } else {
                    return $controller->$action();
                }
            }
        }
        $controller = new controllers\HomeController();
        return $controller->notFound();
    }
}
