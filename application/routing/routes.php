<?php
use routing\Router;

// get routes
Router::get('/', 'HomeController@index');
Router::get('/home', 'HomeController@index');
Router::get('/product/add', 'ProductAddController@show');
Router::get('/product/list', 'ProductListController@show');

// post routes
Router::post('/product/add', 'ProductAddController@insert');
Router::post('/product/list', 'ProductListController@delete');
