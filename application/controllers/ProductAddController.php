<?php namespace controllers;
use models;

class ProductAddController extends Controller
{
    public function show() {
        return $this->view('product/add');
    }

    public function insert($request) {
        $type = $request['productType']; // radio button value from the submitted form determines table/model
        $name = 'models\\'.ucfirst($type);  // cLass name is written with upper case
        $model = new $name($request);
        $message = $model->insert(); // model returns db entry confirmation or error message if entry failed
        return $this->view('product/add', $message);
    }
}
