<?php namespace controllers;

use frontend\View;

abstract class Controller
{
    public function view($name, $data = array()) {
        $view = new View($name, $data);
        return $view->render();
    }
}
