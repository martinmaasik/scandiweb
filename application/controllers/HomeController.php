<?php namespace controllers;

class HomeController extends Controller
{
    public function index() {
        return $this->view('home');
    }

    public function notFound() {
        $request = $_SERVER['REQUEST_URI'];
        return $this->view('home', array('URL' => $request));
    }
}
