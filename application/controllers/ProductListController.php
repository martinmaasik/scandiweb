<?php namespace controllers;
use models\Model;

class ProductListController extends Controller
{
    public function show() {
        $model = new Model();
        $data = $model->getAllProductRows();
        return $this->view('product/list', $data);
    }

    public function delete($request) {
        $model = new Model();
        $model->delete($request);
        return $this->show();
    }
}
